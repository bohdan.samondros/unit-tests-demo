package org.home.example2;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.ignoreStubs;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EntityServiceTest {

    @Mock
    private DatabaseService databaseService;

    @Spy
    private DatabaseServiceImpl databaseServiceSpy;

    @Captor
    private ArgumentCaptor<Long> argumentCaptor;

    private EntityService testedInstance;

    @Before
    public void setUp() {
        testedInstance = new EntityService(databaseServiceSpy);

        when(databaseService.getById(2L)).thenThrow(new IllegalArgumentException());

    }

    @Test
    public void shouldSaveEntityIfNameIsValid() {
        Entity entity = new Entity();
        entity.setName("Vova");

        testedInstance.findById(1L);
        testedInstance.findById(5L);
//        testedInstance.save(entity);

        verify(databaseServiceSpy, atLeastOnce()).getById(argumentCaptor.capture());

        System.out.println(argumentCaptor.getAllValues());

        assertEquals(5L, argumentCaptor.getValue().longValue());
    }

    @Test
    public void shouldFindEntity() {
        Entity entity = new Entity();
        entity.setName("Vasya");

        when(databaseService.getById(1L)).thenReturn(entity);

        Entity actual = testedInstance.findById(1L);

        assertEquals(entity, actual);

        verifyNoMoreInteractions(ignoreStubs(databaseService));
    }

    @Test
    public void shouldFindEntity2() {
        Entity entity = new Entity();
        entity.setName("Vasya");

        when(databaseService.getByNameAndId(eq("Vasya"), anyLong())).thenReturn(entity);

        Entity actual = testedInstance.findByNameAndId("Vasya",3L);

        verify(databaseService).getByNameAndId(anyString(), anyLong());
    }

    @Test
    public void shouldFindEntity3() {
        Entity entity1 = new Entity();
        entity1.setName("Vasya");

        Entity entity2 = new Entity();
        entity2.setName("Vova");

        when(databaseService.getByNameAndId(eq("Vasya"), anyLong())).thenReturn(entity1, entity2);

        Entity actual = testedInstance.findByNameAndId("Vasya",3L);
        System.out.println(actual);

        actual = testedInstance.findByNameAndId("Vasya",3L);
        System.out.println(actual);

//        verify(databaseService).getByNameAndId(anyString(), anyLong());
    }
}