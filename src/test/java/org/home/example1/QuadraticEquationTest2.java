package org.home.example1;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class QuadraticEquationTest2 {

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 0, 5, 6, Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY},
                { 5, 3, 7, Double.NaN, Double.NaN }
        });
    }

    private Number a;
    private Number b;
    private Number c;

    private Double x1;
    private Double x2;

    public QuadraticEquationTest2(Number a, Number b, Number c, Double x1, Double x2) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.x1 = x1;
        this.x2 = x2;
    }

    @Test
    public void shouldTestWithParameters() {
        QuadraticEquation<Number> equation = new QuadraticEquation<>(a, b, c);

        double[] result = equation.getResult();

        assertEquals(x1, result[0], 0.1);
        assertEquals(x2, result[1], 0.1);
    }
}