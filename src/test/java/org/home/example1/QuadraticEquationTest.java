package org.home.example1;

import org.junit.Test;

import static org.junit.Assert.*;

public class QuadraticEquationTest {

    @Test
    public void shouldReturnInfinityWhenAIsZero() {
        QuadraticEquation<Integer> equation = new QuadraticEquation<>(0, 5, 6);

        double[] result = equation.getResult();

        assertEquals(Double.POSITIVE_INFINITY, result[0], 0.1);
        assertEquals(Double.NEGATIVE_INFINITY, result[1], 0.1);
    }

    @Test
    public void shouldReturnNaNWhenDiscriminantIsNegative() {
        QuadraticEquation<Integer> equation = new QuadraticEquation<>(5, 3, 7);

        double[] result = equation.getResult();

        assertEquals(Double.NaN, result[0], 0.1);
        assertEquals(Double.NaN, result[1], 0.1);
    }
}