package org.home.homework;

/**
 * Объект данного типа должен уметь отображать прогноз погоды на ближайшие 7 дней
 *
 * Он берет данные у Storage и выводит их в консоль в удобно читаемом для пользователя формате
 */
public interface Predictor {
    void showPrediction();
}
