package org.home.homework;

/**
 * Объект служит для преобразования внешних данных во внутренние данные
 */
public interface DataConverter {
    InternalData convert(ProvidedData source);
}
