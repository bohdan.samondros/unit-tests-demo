package org.home.example1;

import java.math.BigDecimal;
import java.util.Arrays;

import static java.lang.Double.NaN;

/**
 * a*x^2 + b*x + c
 * @param <T>
 */

public class QuadraticEquation<T extends Number> {
    private final T a;
    private final T b;
    private final T c;

    public QuadraticEquation(T a, T b, T c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    double getDiscriminant() {
        return Math.pow(b.doubleValue(), 2) - 4 * a.doubleValue() * c.doubleValue();
    }

    public double[] getResult() {
        double x1, x2;
        if (getDiscriminant() < 0) {
            x1 = x2 = NaN;
        } else {
            x1 = (- b.doubleValue() + getDiscriminant()) / (2 * a.doubleValue());
            x2 = (- b.doubleValue() - getDiscriminant()) / (2 * a.doubleValue());
        }

        return new double[] { x1, x2 };
    }

    public static void main(String[] args) {
        QuadraticEquation<Integer> qq = new QuadraticEquation<>(2, 10, 1);
        Arrays.stream(qq.getResult()).forEach(System.out::println);
        System.out.println("=====");
        QuadraticEquation<BigDecimal> qq1 = new QuadraticEquation<>(BigDecimal.valueOf(2), BigDecimal.valueOf(10), BigDecimal.valueOf(1));
        Arrays.stream(qq1.getResult()).forEach(System.out::println);
        System.out.println("=====");
        QuadraticEquation<Long> qq2 = new QuadraticEquation<>(1L, -6L, 9L);
        System.out.println(qq2.getDiscriminant());
        Arrays.stream(qq2.getResult()).forEach(System.out::println);
        System.out.println("=====");
        QuadraticEquation<Long> qq3 = new QuadraticEquation<>(5L, 3L, 7L);
        System.out.println(qq3.getDiscriminant());
        Arrays.stream(qq3.getResult()).forEach(System.out::println);
    }
}
