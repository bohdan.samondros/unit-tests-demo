package org.home.example2;

public class EntityService {
    private final DatabaseService databaseService;

    public EntityService(DatabaseService databaseService) {
        this.databaseService = databaseService;
    }

    public void save(Entity entity) {
        if (validate(entity)) {
             databaseService.save(entity);
        } else {
            throw new IllegalArgumentException("Entity is not valid");
        }
    }

    public Entity findById(long id) {
        return databaseService.getById(id);
    }

    public Entity findByNameAndId(String name, long id) {
        return databaseService.getByNameAndId(name, id);
    }

    private boolean validate(Entity entity) {
        // validation logic here
        return entity != null && entity.getName() != null && entity.getName().length() > 3;
    }
}
