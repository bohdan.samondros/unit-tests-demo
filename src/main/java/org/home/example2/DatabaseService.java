package org.home.example2;

public interface DatabaseService {
    void save(Entity entity);

    Entity getById(long id);

    Entity getByNameAndId(String name, long id);
}
