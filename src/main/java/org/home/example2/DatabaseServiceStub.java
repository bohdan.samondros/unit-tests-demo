package org.home.example2;

public class DatabaseServiceStub implements DatabaseService {

    int counterSave;

    @Override
    public void save(Entity entity) {
        counterSave++;
    }

    @Override
    public Entity getById(long id) {
        return null;
    }

    @Override
    public Entity getByNameAndId(String name, long id) {
        return null;
    }

    public int getCounterSave() {
        return counterSave;
    }
}
